from rest_framework import viewsets, status

from new_project.blog import error_codes
from new_project.blog.models import Blog
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from new_project.blog.serializers import BlogSerializer


class BlogListViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    # authentication_classes = (OAuth2Authentication,)
    queryset = Blog.objects.all()
    serializer_class = BlogSerializer

    def get_object(self, pk):
        return Blog.objects.get(pk=pk)

    def retrieve(self, request, pk=None):
        try:
            blog = self.get_object(pk)
            serializer = BlogSerializer(blog)
            return Response(serializer.data)
        except Blog.DoesNotExist:
            return Response("not")
        except Exception as e:
            return Response("not")


class BlogViewSet(viewsets.ModelViewSet):
    queryset = Blog.objects.all()
    permission_classes = (IsAuthenticated,)
    authentication_classes = (JSONWebTokenAuthentication,)
    serializer_class = BlogSerializer

    def get_object(self, pk):
        return Blog.objects.get(pk=pk)

    def create(self, request):
        try:
            title = request.data['title']
            description = request.data['description']
            owner = request.user
            blog = Blog.objects.create(title=title, description=description,
                                       owner=owner)
            serializer = self.serializer_class(blog)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        except Blog.DoesNotExist:
            return Response("not")
        except Exception as e:
            return Response("not")

    def destroy(self, request, pk):
        try:
            instance = self.get_object(pk)
            user = request.user
            if user == instance.owner:
                self.perform_destroy(instance)
                return Response({'message': "Blog has been deleted", 'status': status.HTTP_201_CREATED})
            else:
                return Response({"message": "only owner can delete the blog", "status": status.HTTP_401_UNAUTHORIZED})
        except Blog.DoesNotExist:
            return Response("not")

    def update(self, request, pk):
        try:
            instance = self.get_object(pk)
            if request.user == instance.owner:
                instance.title = request.data['title']
                instance.description = request.data['description']
                instance.save()
                serializer = BlogSerializer(instance)
                return Response(serializer.data)
            else:
                return Response({"message": "only owner can update the blog", "status": status.HTTP_401_UNAUTHORIZED})
        except Blog.DoesNotExist:
            return Response("not")
        except Exception as e:
            return Response("not")

